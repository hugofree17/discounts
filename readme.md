# Discount API

Used to calculate discounts to a given order.

Returns the same order with updated values.

API currently in version 1.

## Installation
```
git clone https://hugofree17@bitbucket.org/hugofree17/discounts.git
copy .env.example to .env
add API_VERSION=1 to .env
composer update
composer require guzzlehttp/guzzle
```

## Understanding the API (Directory Structure)
Everything is stored in app/Api/V1

### Data
You can find here customers and products json.
This are fake data to simulate an external API and test cases

### Discounts
This is where should be stored the discount logics.

Also there is a **Actives.php** file containing the active discount logics.

### Handlers
Handler the requests and responses.
Can be called as a Service as well.

Does the communication between the controllers and each discount logic.

### Helpers
Helper classes to use in the application.
Thinking in DRY, organization and separate logics.

## Using the API
Locally it's ready to use.
Just need to make a POST request giving the order as parameter and it will be returned the same order with updated values.

To use with a real external API it's only needed some changes:

in **app/Api/V1/Helpers/DiscountHelper.php**

Change the baseUrl property to the base url of the API

Also the productRoute and customerRoute to the respectively routes.

## Unit Testing
You can find the unit testing to this API in **tests/DiscountsTest.php**

There is a test for each discount logic and another for no discounts.

#### Please any question feel free to ask.