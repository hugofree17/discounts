<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'api'], function () use ($app) {
	$app->get('/', 'ApiController@apiVersion');
	$app->post('/', 'ApiController@apiVersion');

	$app->get('external/product/attributes', [
    'as' => 'productAttr', 'uses' => 'ExternalApiController@getProductAttr'
	]);

	$app->get('external/customer/attributes', [
    'as' => 'customerAttr', 'uses' => 'ExternalApiController@getCustomerAttr'
	]);
	
	$app->post('discount', 'ApiController@discount');
});