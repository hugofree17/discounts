<?php

class DiscountsTest extends TestCase
{
    /**
     * A test to get the current version of the API.
     *
     * @return void
     */
    public function testGet_API_Version()
    {
        $this->get('/api');
        $content = json_decode($this->response->getContent(), true);

        $this->assertEquals(
            env('API_VERSION'), $content['version']
        );
    }

    public function testPost_Discount1()
    {
        $this->json('POST', '/api/discount', 
            ['id'=> '3',
            'customer-id'=> '2',
            'items'=> [ 
                [
                    'product-id'=> 'A101',
                    'quantity'=> '5',
                    'unit-price'=> '9.75',
                    'total'=> '48.75'
                ],
                [
                    'product-id'=> 'A102',
                    'quantity'=> '1',
                    'unit-price'=> '49.50',
                    'total'=> '49.50'
                ]
            ],
            'total'=> '98.25'])
             ->seeJsonEquals([
            'id'=> '3',
            'customer-id'=> '2',
            'items'=> [ 
                [
                    'product-id'=> 'A101',
                    'quantity'=> '5',
                    'unit-price'=> '9.75',
                    'total'=> '48.75'
                ],
                [
                    'product-id'=> 'A102',
                    'quantity'=> '1',
                    'unit-price'=> '49.50',
                    'total'=> '49.50'
                ]
            ],
            'total'=> '98.25',
            'total_with_discount'=> '88.42']);
    }

    public function testPost_Discount2()
    {
        $this->json('POST', '/api/discount', 
            ['id'=> '4',
            'customer-id'=> '3',
            'items'=> [ 
                [
                    'product-id'=> 'B101',
                    'quantity'=> '5',
                    'unit-price'=> '4.99',
                    'total'=> '24.95'
                ],
                [
                    'product-id'=> 'A102',
                    'quantity'=> '1',
                    'unit-price'=> '49.50',
                    'total'=> '49.50'
                ]
            ],
            'total'=> '74.45'])
             ->seeJsonEquals([
            'id'=> '4',
            'customer-id'=> '3',
            'items'=> [ 
                [
                    'product-id'=> 'B101',
                    'quantity'=> '6',
                    'unit-price'=> '4.99',
                    'total'=> '29.94',
                    'total_with_discount'=> '24.95'
                ],
                [
                    'product-id'=> 'A102',
                    'quantity'=> '1',
                    'unit-price'=> '49.50',
                    'total'=> '49.50'
                ]
            ],
            'total'=> '79.44',
            'total_with_discount'=> '74.45']);
    }

    public function testPost_Discount3()
    {
       $this->json('POST', '/api/discount', 
            ['id'=> '5',
            'customer-id'=> '3',
            'items'=> [ 
                [
                    'product-id'=> 'A101',
                    'quantity'=> '5',
                    'unit-price'=> '9.75',
                    'total'=> '48.75'
                ],
                [
                    'product-id'=> 'A102',
                    'quantity'=> '1',
                    'unit-price'=> '49.50',
                    'total'=> '49.50'
                ]
            ],
            'total'=> '98.25'])
             ->seeJsonEquals([
            'id'=> '5',
            'customer-id'=> '3',
            'items'=> [ 
                [
                    'product-id'=> 'A101',
                    'quantity'=> '5',
                    'unit-price'=> '9.75',
                    'total'=> '48.75',
                    'total_with_discount'=> '46.8'
                ],
                [
                    'product-id'=> 'A102',
                    'quantity'=> '1',
                    'unit-price'=> '49.50',
                    'total'=> '49.50'
                ]
            ],
            'total'=> '98.25',
            'total_with_discount'=> '96.3']);
    }

    public function testPost_No_Discount()
    {
        $this->json('POST', '/api/discount', 
            ['id'=> '6',
            'customer-id'=> '3',
            'items'=> [ 
                [
                    'product-id'=> 'B101',
                    'quantity'=> '1',
                    'unit-price'=> '9.75',
                    'total'=> '9.75'
                ],
                [
                    'product-id'=> 'A102',
                    'quantity'=> '1',
                    'unit-price'=> '49.50',
                    'total'=> '49.50'
                ]
            ],
            'total'=> '59.25'])
             ->seeJsonEquals([
            'id'=> '6',
            'customer-id'=> '3',
            'items'=> [ 
                [
                    'product-id'=> 'B101',
                    'quantity'=> '1',
                    'unit-price'=> '9.75',
                    'total'=> '9.75'
                ],
                [
                    'product-id'=> 'A102',
                    'quantity'=> '1',
                    'unit-price'=> '49.50',
                    'total'=> '49.50'
                ]
            ],
            'total'=> '59.25']);
    }
}
