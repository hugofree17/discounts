<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Api\V1\Handlers\DiscountHandler;

/**
 * Api Controller
 * Manages Api requests and responses
 */
class ApiController extends BaseController
{
	/**
	 * Current version of the Api
	 */
	private $version;

	/**
	 * DiscountHandler
	 * Used to search and apply discounts
	 */
	private $discountHandler;

	function __construct(DiscountHandler $discountHandler) {
		$this->version = env('API_VERSION');
		
		$this->discountHandler = $discountHandler;
	}

	/**
	 * Returns current version of the Api
	 */
    public function apiVersion(){
    	return response()->json(['version' => $this->version]);
    }

    /**
     * Receives a request(order)
     * and returns the order with the applied discount
     */
    public function discount(Request $request){
    	$response = $this->discountHandler->calculateDiscount($request);
    	return response()->json($response);
    }
}
