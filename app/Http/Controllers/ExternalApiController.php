<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Api\V1\Handlers\ExternalApiHandler;

/**
 * External Api Controller
 * Manages a fake Api requests and responses
 */
class ExternalApiController extends BaseController
{
	/**
	 * ExternalApiHandler
	 * Used to return data from the fake Api
	 */
	private $apiHandler;

	function __construct(ExternalApiHandler $apiHandler) {
		$this->apiHandler = $apiHandler;
	}

	/**
	 * Gets a product id and returns the wanted attributes
	 */
	public function getProductAttr(Request $request){
		$id = $request->input('id');
		$attributes = $request->input('attributes');
		
		return $this->apiHandler->getProductAttr($id, $attributes);
	}

	/**
	 * Gets a customer id and returns the wanted attributes
	 */
	public function getCustomerAttr(Request $request){
		$id = $request->input('id');
		$attributes = $request->input('attributes');
		
		return $this->apiHandler->getCustomerAttr($id, $attributes);
	}
}
