<?php

namespace App\Api\V1\Helpers;

use App\Api\V1\Helpers\BaseHelper;

class DiscountHelper extends BaseHelper
{
    private static $baseUrl = '';
    private static $productRoute = 'productAttr';
    private static $customerRoute = 'customerAttr';
    /**
     * Calculates the amount of products offered
     * based on the quantity of products in the cart,
     * quantity needed to have a bonus
     * and bonus amount
     */
    public static function calculateDiscountOffer($quantity, $quantityToOffer, $bonus){
        return (int)($quantity * $bonus / $quantityToOffer);
    }

    /**
     * Calculates the amount of the discount
     */
    public static function calculateDiscountAmount($value, $discount_percentage){
        return round(($value * $discount_percentage), 2);
    }

    /**
     * Gets an items array(from a order) and categories wanted
     * checks the category of each product
     * if it's in the wanted categories
     * returns the unit price of the product
     * and the quantity inside the items array
     */
    public static function getItemsInfoOfCategories($items, $categories){
        $info['products'] = array();
        $info['counter'] = 0;

        $url = self::constructUrl(self::$baseUrl, self::$productRoute);

        foreach ($items as $key => $item) {
            $data = ['id' => $item['product-id'], 'attributes' => ['category']];
            $content = self::request('GET', $url, $data);
            
            if(is_null($content) || !array_key_exists('category', $content) || !in_array($content['category'], $categories)){
                continue;
            }

            $info['products'][$key] = $item['unit-price'];
            $info['counter'] += $item['quantity'];
        }
        return $info;
    }

    /**
     * Get a customerId and
     * returns how much a customer has spend in the store
     */
    public static function getRevenueOfCustomer($customerId){
        $url = self::constructUrl(self::$baseUrl, self::$customerRoute);
        $data = ['id' => $customerId, 'attributes' => ['revenue']];
        $content = self::request('GET', $url, $data);
        return $content['revenue'];
    }
}
