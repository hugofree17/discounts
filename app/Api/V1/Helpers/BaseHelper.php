<?php

namespace App\Api\V1\Helpers;

use GuzzleHttp\Client;

class BaseHelper
{
    /**
     * GuzzleHttp client to make requests
     */
    public static $client;

    /**
     * Makes a api request
     * and returns the json decoded response
     */
    public static function request($method, $url, $data){
        self::$client = new Client();
        $response = self::$client->request($method, $url, ['json' => $data]);
        return json_decode($response->getBody()->getContents(), true);
    }

    public static function constructUrl($baseUrl, $route){
    	return $baseUrl === '' 
    		? route($route)
    		: $baseUrl.$route;
    }
}
