<?php

namespace App\Api\V1\Handlers;
use App\Api\V1\Handlers\BaseHandler;

class ExternalApiHandler extends BaseHandler
{
    /**
     * Base Path where fake data is
     */
    private $basePath = __DIR__.'/../Data/';

    /**
     * Products list
     */
    private $products;
    private $productsPath = 'products.json';

    /**
     * Customers list
     */
    private $customers;
    private $customersPath = 'customers.json';

    /**
     * Loads products and customers
     */
    public function __construct(){
        $path = realpath($this->basePath . $this->productsPath);
        $this->products = json_decode(file_get_contents($path), true);
        
        $path = realpath($this->basePath . $this->customersPath);
        $this->customers = json_decode(file_get_contents($path), true);
    }

    /**
     * Returns the wanted attributes from a given product id
     * if no attributes are passed, all attributes are returned
     */
    public function getProductAttr($id, $attributes){
        if(is_null($id)){
            return $id;
        }

        $product_index = array_search($id, array_column($this->products, 'id'));
        if($product_index === false){
            return [];
        }

        if(is_null($attributes)){
            return $this->products[$product_index];
        }

        return $this->getAttributes($this->products[$product_index], $attributes);
    }

    /**
     * Returns the wanted attributes from a given customer id
     * if no attributes are passed, all attributes are returned
     */
    public function getCustomerAttr($id, $attributes){
        if(is_null($id)){
            return $id;
        }

        $customer_index = array_search($id, array_column($this->customers, 'id'));
        if($customer_index === false){
            return [];
        }

        if(is_null($attributes)){
            return $this->customers[$customer_index];
        }
        
        return $this->getAttributes($this->customers[$customer_index], $attributes);
    }
}
