<?php

namespace App\Api\V1\Handlers;

class BaseHandler
{

    public function __construct(){

    }

    /**
     * Loads output of file and returns it
     * so it can be used as a variable
     */
    public function load($file){
    	return include $file;
    }

    /**
     * Return wanted attributes from passed array
     */
    public function getAttributes($data, $attributes){
        $attrs = [];
        foreach ($data as $key => $value) {
            if(in_array($key, $attributes)){
                $attrs[$key] = $value;
            }
        }
        return $attrs;
    }
}
