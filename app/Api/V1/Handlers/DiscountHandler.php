<?php

namespace App\Api\V1\Handlers;
use App\Api\V1\Handlers\BaseHandler;

class DiscountHandler extends BaseHandler
{
    /**
     * Path to load the active discounts 
     */
    private $discountsPath = __DIR__.'/../Discounts/Actives.php';

    /**
     * Active Discounts that can be applied
     */
    private $activeDiscounts;

    public function __construct(){
        $this->activeDiscounts = $this->load($this->discountsPath);
    }

    /**
     * It iterates every active discount and applies if matches.
     * Once applied returns the order back with new values.
     */
    public function calculateDiscount($order){
        foreach ($this->activeDiscounts as $discountType) {
            $newDiscount = new $discountType();
            $order = $newDiscount->calculateDiscount($order);

            if(isset($order['total_with_discount'])){
                break;
            }
        }
        
        return $order;
    }
}
