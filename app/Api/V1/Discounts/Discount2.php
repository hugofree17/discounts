<?php

namespace App\Api\V1\Discounts;

use App\Api\V1\Discounts\iDiscount;
use App\Api\V1\Helpers\DiscountHelper;

class Discount2 implements iDiscount
{
    /**
     * Categories to be applied the discount
     */
    private $categories = [2];

    /**
     * Needed quantity to get the bonus
     */
    private $quantity = 5;

    /**
     * The quantity bonus that is given for every $quantity products of the $categories
     */
    private $bonus = 1;

    public function __construct()
    {

    }

    /**
     * Gives 1 product bonus for each 5 products of the category 2
     */
    public function calculateDiscount($order){
        $items = $order['items'];
        
        $itemsInfo = DiscountHelper::getItemsInfoOfCategories($items, $this->categories);

        if($itemsInfo['counter'] < $this->quantity){
            return $order;
        }
        
        $key = array_search(min($itemsInfo['products']), $itemsInfo['products']);
        $offer = DiscountHelper::calculateDiscountOffer($itemsInfo['counter'], $this->quantity, $this->bonus);

        $items[$key]['quantity'] += $offer;
        $items[$key]['quantity'] = (string)$items[$key]['quantity'];
        $items[$key]['total_with_discount'] = $items[$key]['total'];
        $items[$key]['total'] = (string)($items[$key]['quantity'] * $items[$key]['unit-price']);
        $order['items'] = $items;

        $order['total_with_discount'] = $order['total'];
        $order['total'] += ($items[$key]['total'] - $items[$key]['total_with_discount']);
        $order['total'] = (string)round($order['total'], 2);

        return $order;
    }
}
