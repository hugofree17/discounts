<?php

namespace App\Api\V1\Discounts;

interface iDiscount
{
    public function calculateDiscount($order);
}
