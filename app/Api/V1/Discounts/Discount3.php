<?php

namespace App\Api\V1\Discounts;

use App\Api\V1\Discounts\iDiscount;
use App\Api\V1\Helpers\DiscountHelper;

class Discount3 implements iDiscount
{
    /**
     * Categories to be applied the discount
     */
    private $categories = [1];

    /**
     * Needed quantity to get the discount
     */
    private $quantity = 2;

    /**
     * The percentage of the discount to the whole order
     * It should be put in decimal
     * ex: 20% = 0.2
     */
    private $discount_percentage = 0.2;

    public function __construct()
    {

    }

    /**
     * If there is at least 2 products of the category 1
     * you get a 20% discount in the cheapest one
     */
    public function calculateDiscount($order){
        $items = $order['items'];
        
        $itemsInfo = DiscountHelper::getItemsInfoOfCategories($items, $this->categories);

        if($itemsInfo['counter'] < $this->quantity){
            return $order;
        }

        $lowestPrice = min($itemsInfo['products']);

        $key = array_search($lowestPrice, $itemsInfo['products']);
        
        $discountAmount = DiscountHelper::calculateDiscountAmount($lowestPrice, $this->discount_percentage);

        $items[$key]['total_with_discount'] = (string)round($items[$key]['total'] - $discountAmount, 2);
        $order['items'] = $items;

        $order['total_with_discount'] = (string)round($order['total'] - $discountAmount, 2);

        return $order;
    }
}
