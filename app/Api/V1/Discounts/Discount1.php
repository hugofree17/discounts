<?php

namespace App\Api\V1\Discounts;

use App\Api\V1\Discounts\iDiscount;
use App\Api\V1\Helpers\DiscountHelper;

class Discount1 implements iDiscount
{
    /**
     * The total amount that is to start applying the discount
     */
    private $total_amount = 1000;

    /**
     * The percentage of the discount
     * It should be put in decimal
     * ex: 10% = 0.1
     */
    private $discount_percentage = 0.1;

    public function __construct()
    {

    }

    /**
     * If the customer already bought for over 1000€
     * gets a 10% discount in the total order
     */
    public function calculateDiscount($order){
        $revenue = DiscountHelper::getRevenueOfCustomer($order['customer-id']);
        if($revenue <= $this->total_amount){
            return $order;
        }

        $discount_amount = 
            DiscountHelper::calculateDiscountAmount($order['total'], $this->discount_percentage);
        $order['total_with_discount'] = (string)round($order['total'] - $discount_amount, 2);

        return $order;
    }
}
